---
layout: handbook-page-toc
title: "Technical Writing Fundamentals"
description: "Get started working with GitLab product documentation with our Technical Writing Fundamentals."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Technical Writing Fundamentals course is available to help GitLab and community contributors document product features. This course provides direction on grammar, tooling, and topic design. 

## Technical Writing Fundamentals course

The Technical Writing Fundamentals course consists of the following sections, which you should complete in order:

1. [**Pre-class work**](https://developers.google.com/tech-writing/one): Complete the pre-class exercises from this excellent Google training. You can take the associated Google class if you like, but the following sessions cover the information that is relevant for GitLab.
1. [**Full slide deck**](https://docs.google.com/presentation/d/1cT4r32EBs--x0FdynDa7k8-fNyX5QqtBVpcj7dXI45k/edit?usp=sharing): Follow along with recorded training sessions.
  - [**Session 1**]: Introduction, audience for the documentation, brief recap of pre-class work, grammar and style requirements. _Video: coming soon!_
  - [**Session 2**]: Additional grammar and style requirements. _Video: coming soon!_
  - [**Session 3**]: Continued grammar and style requirements, linting. _Video: coming soon!_
  - [**Session 4**]: Concepts, tasks, references, troubleshooting (CTRT) topic types. _Video: coming soon!_

## Schedule in-person training

While we emphasize asynchronous training, in-person training for GitLab team members is available on a limited basis. Contact Susan Tacker (`@susantacker` in Slack) to schedule training sessions.
