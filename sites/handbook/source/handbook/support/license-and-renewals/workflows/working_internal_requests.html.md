---
layout: handbook-page-toc
title: Internal Requests tracker
category: Working L&R requests
description: Describes how to service internal requests for licensing & renewals.
---

{:.no_toc}

----

### Servicing Internal Requests

Follow the same [General Workflow](/handbook/support/workflows/internal_requests.html#general-workflow) as dotcom for servicing L&R internal requests.

### L&R Internal Requests Hawk

There is a internal requests hawk role which rotates on a weekly basis which
serves as a temporary measure to make sure internal requests tagged with
the following labels receive the appropriate attention from Support:

* `Associate Subscription Console Escalation::Customers` - Workflow to be documented
* `Billable Members` - Workflow instructions on template
* `Console Escalation::Customers` - Workflow to be documented
* `EULA` - [Workflow](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/troubleshooting_eulas.html)
* `License Issue` - [Workflow](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/license_troubleshooting.html)
* `Plan Change Request` - [Workflow](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/saas/plan_change_requests.html)
* `SaaS Subscription Issue` - [Workflow](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/license_troubleshooting.html#transactions)
* `Trial Extension License issue` - [Workflow](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/saas/trials.html#extending-trials)

When on this role, prioritize working on internal request issues over Zendesk tickets.
The general workflow guide and best practices are:

1. Review the list of [unassigned issues](https://gitlab.com/gitlab-com/support/internal-requests/-/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_id=None&not[label_name][]=Console%20Escalation%3A%3AGitLab.com&not[label_name][]=DEWR).
1. Pick an issue to work on/respond to and assign this to yourself.
1. If you have resolved the problem or attended to the request, close the issue and let the requestor know to re-open the issue if further assistance is required.
1. If you are awaiting manager approval or a reply from the requestor before moving the issue forward, add the appropriate labels (for example `Manager Approval::Awaiting` or `Status::Awaiting Confirmation`).
1. Repeat steps above until there are no longer any unassigned issues.
1. Check if there are any [assigned issues where responses have stalled](https://gitlab.com/gitlab-com/support/internal-requests/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not[label_name][]=Console%20Escalation%3A%3AGitLab.com&not[label_name][]=DEWR&assignee_id=Any) and check with the assignee and requestor if follow up action needs to be taken.

You will continue to be responsible for any issues assigned to yourself even
after you're off the role, so be mindful about working sustainably. If you need
to, ask for others to help with unassigned issues -- the expectation is that
GitLab Support as a team should be responding timely to internal requests, not
you personally as the L&R Internal Requests Hawk.

You'll only be expected to work your usual work hours while doing this role. There
is a [PagerDuty schedule](https://gitlab.pagerduty.com/schedules#PQ6DB1G) set up
solely to facilitate tracking/swapping of shifts. It is in no way an indication of
actual hours of work.
