---
layout: handbook-page-toc
title: "Brand Guidelines"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Brand Guidelines
{:.no_toc}

### Brand Purpose
- [Vision](https://about.gitlab.com/direction/#vision) 
- [Mission](https://about.gitlab.com/company/mission/#mission)
- [Values](https://about.gitlab.com/handbook/values/)

### Brand Standards
Last updated: 2021-02-01
<html>
  <head>
    <title>PDF Viewer</title>
  </head>
  <body>
    <div>
      <object
        data='/resources/gitlab-brand/gitlab-brand-standards.pdf'
        type="application/pdf"
        width="820"
        height="650"
      >

        <iframe
          src='/resources/gitlab-brand/gitlab-brand-standards.pdf'
          width="820"
          height="650"
        >
        <p>This browser does not support PDF!</p>
        </iframe>

      </object>
    </div>

  </body>
</html>

 ### Tone of voice (TOV)

The following guide outlines the set of standards used for all written company
communications to ensure consistency in voice, style, and personality, across all
of GitLab's public communications.

The tone of voice we use when speaking as GitLab should always be informed by
our [mission & vision](https://about.gitlab.com/company/mission/#mission). We use [inclusive, gender-neutral language](https://litreactor.com/columns/5-easy-ways-to-make-your-writing-gender-neutral).
Most importantly, we see our audience as co-conspirators, working together to
define and create the next generation of software development practices. The below
presentation should help to clarify further:

<figure class="video_container"><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQnRSaLsmAyGxcDHj3a_uh5UT2h45WUKqF0UGwtedOVGUK0iCcC134czSZ_Hv7cOvLF9BpKM0_frZMS/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe></figure>

Watch a walkthrough of the TOV project and guidelines - [GitLab's Tone-of-voice: Seat at the Table](https://youtu.be/j-viaR6qhXM) 

See [the Blog Editorial Style Guide](/handbook/marketing/inbound-marketing/content/editorial-team/) for more.
