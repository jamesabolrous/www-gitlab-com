---
layout: handbook-page-toc
title: "Security Awareness Training Program"
description: "Security Awareness Training Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Security Awareness Training Program

The GitLab security awareness training program provides ongoing trainings to GitLab team members that enhances knowledge and identification of cybersecurity threats as well as helps identify social engineering vulnerabilties where additional education is provided.  Security Awareness Trainings are provided by KnowBe4, GitLab's contracted solution that will help satisfy external regulatory requirements and bolster customer assurance.

# Overview
GitLab's security governance discipline helps to define, train and measure security strategies and progress toward security objectives by creating a set of processes and practices that run across departments and functions.  

Security awareness training seeks to educate GitLab team-members with the information they need to protect themselves and GitLab from loss or harm.  The purpose of the annual training is to mature our internal security posture through regular training while satisfying external compliance and regulatory requirements. 

## When will security awareness trainings occur?

General Security Awareness training will occur annually in Q2 of each fiscal year. 

Prior to the security awareness training taking place, a general notification to the GitLab organization will be posted to the `#whats-happening-at-gitlab` Slack channel.

## Who will receive the annual security awareness training?

All GitLab team-members should be aware of the importance of their role in securing GitLab on a daily basis, and to empower them to make the right decisions with security best-practices.  All employees of the GitLab organization hired prior to June 1 of the current year will receive an email from our training vendor, KnowBe4. All GitLab employees hired after June 1 of the current year will have just completed their New Hire Security Awareness Training and therefore will not be requried to take the annual security awareness training until the following year.

## How long with the training take? 

The security awareness training has been limited to 30 minutes in an effort to find the best return of security investment from team-member's time.  

## What will be covered in the training?

There will be a GitLab-specific introduction module followed by industry-standard training via KnowBe4.  There will be a short quiz to identify what you have learned.

* Special topics covered:
    * [Suspected phishing](https://about.gitlab.com/handbook/security/#phishing-tests)
    * [Acceptable Use](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/)
    * [Device Lost or Stolen?!](https://about.gitlab.com/handbook/security/#panic-email)
        * email: panic@gitlab.com
    * [Data Classification](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html)
    * [No Red Data on Unapproved Locations](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/#security-and-proprietary-information)

## What happens if training is not completed?

The training will be available for 30 days.  If the training is not completed, Security Assurance will send weekly reminder notifications requesting completion of the training.  
If required, we will communicate incomplete assigned trainings to managers for assistance with completion.  Demonstration of a completed training supports compliance with the Security Awareness Training program and will strengthen our regulatory requirements.

### Security Awareness Training Metrics

The Security Compliance team will track the annual security awareness training completion rate in the GitLab ZenGRC instance.  Once the training campaign has completed, the Security Compliance team will provide results in the [Security Awareness Training Program](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/security-awareness-training-program) project.

### Questions and Answers

*Why are we using an external vendor?*

* While GitLab's learning platform is being built out it doesn't yet have the features required to support security training requirements, the decision was made to utilize an external solution that would be able to provide a consistent process that we could rely on.  As we had previously used KnowBe4 to run our FY21 Security Awareness Training campaign, we decided to continue partnering with them. The goal is to move all GitLab security awareness training content into EdCast as soon as the platform is able to support this training.

*Why was I chosen?*

* All GitLab team members will be required to complete our security awareness trainings whether it be during New Hire Orientation or annually. 

*I don't want to be included, how do I remove myself?*

* All GitLab team members have the responsiblity to help keep themselves, team members, the company and the customer secure; all are required to complete assigned training.

*Will I be publicly shamed?*

* No, we will never post or create metrics which will associate a team member success or failure with a training.  If we release metrics company-wide, we will generate non-identifying metrics to be shared internally and public-facing.

*How can I provide Feedback on my experience?*

* Please feel free to add any feedback, comments, concerns on this [feedback issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/-/issues/2630).

### Additional Questions, Comments, Concerns?

Please reach out to the [Security Compliance team!](/security-assurance/security-compliance/compliance.html#contact-the-compliance-team)
